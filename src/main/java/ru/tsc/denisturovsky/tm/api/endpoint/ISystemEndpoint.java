package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.ServerAboutRequest;
import ru.tsc.denisturovsky.tm.dto.request.ServerVersionRequest;
import ru.tsc.denisturovsky.tm.dto.response.ServerAboutResponse;
import ru.tsc.denisturovsky.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
